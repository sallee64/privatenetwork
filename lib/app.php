<?php
namespace Lib;


class App 
{
    protected static $config=[];
    
    
    
    
    
    
    
    public static function getConfig() {
        if (empty(self::$config)) {
            self::$config = include(__DIR__.'/.config.php');
        }
        return self::$config;
    }
    
    
    public static function autoloadClass($className) {
		self::loadClassFile($className);
	}
	
	
	
	protected static function loadClassFile($fullClassName) {
		$arNameParts = explode('\\', $fullClassName);
		$classPart = array_pop($arNameParts);
		$classPart = preg_replace('/(.)([A-Z])(.)/','$1_$2$3', $classPart);
		$classFileName = strtolower($classPart).'.php';
		$filePath = $_SERVER['DOCUMENT_ROOT'];
		foreach($arNameParts as $part) {
			$part = strtolower($part);
			if (strlen($part))
				$filePath .= '/'.$part;
		}
		$fileFullName = $filePath.'/'.$classFileName;
		if (is_readable($fileFullName)) {
			require_once($fileFullName);
			return true;
		}
		return false;
	}
	
	
	// LOGS % % % % % % % % % % % % % % % % % % % % % % % % % % % %
	
	public static function log($msg, $suffix='main', $clean = false) {
		if (is_string($msg)) {
			$DATE          = date('Y-m-d H:i:s');
			$strCalledFrom = '';
			if (function_exists('debug_backtrace')) {
				$locations     = debug_backtrace();
				foreach($locations as $item) {
					$strCalledFrom .= 'F: ' . $item['file'] . '  --   L: ' . $item['line'] . "\n";
				}
			}
			$logMsg = "\n" .
				'date: ' . $DATE . "\n" .
				'mess: ' . $msg . "\n" .
				'from: ' . $strCalledFrom . "\n" .
				'uri : ' . $_SERVER['REQUEST_URI'] . "\n" .
				'----------------------------------------------------------';
			if ($clean) {
				self::CleanLog($suffix);
			}
			self::AppendLog($logMsg, $suffix);
		}
	}

	private static function AppendLog($msg, $suffix='main') {
		if ($fp = fopen(self::getLogFileName($suffix), 'ab')) {
			fwrite($fp, $msg);
			fclose($fp);
		}
	}

	private static function CleanLog($suffix='main') {
		@unlink(self::getLogFileName($suffix));
	}

	private static function getLogFileName($suffix='main') {
		static $arFileNames;
		if (!is_array($arFileNames) || empty($arFileNames[$suffix])) {
			$arFileNames[$suffix] = $_SERVER['DOCUMENT_ROOT'].'/logs/';
			$arFileNames[$suffix] .= 'log_'.$suffix.'.txt';
		}
		return $arFileNames[$suffix];
	}
    
    // /LOGS % % % % % % % % % % % % % % % % % % % % % % % % % % % %
}
