<?php
/**
* COrm
*
* Общий интерфейс с базой данных
* Использует mysqli
* Работает на основе конфигурационного файла .config_orm.php
* @package privatenetwork
* @namespace \Lib\Model
* @constant string PAGER_VAR_NAME префикс переменной запроса, передающей текущее положение в постраничке
* @copyright Copyright (c) 2020 Vadim Soluyanov 
* @author Vadim Soluyanov 
* @access public
*/
namespace Lib\Model;

if (!defined('PAGER_VAR_NAME')) {
	define('PAGER_VAR_NAME','pager');
}

/**
* Orm
*
* Интерфейс с базой. Работает на основе конфига данных из файла .config_orm.php
* Использует дополнительные классы для работы с базой:
* Result, Table. Не стоит их использовать напрямую, хотя... хз.
*/
class Orm
{
    const SEARCH_INDEX_TABLE = 'pn_search_index';
    
    
    
	/**
	* @internal
	*/
	private static $DB = null;

	/**
	* @internal
	*/
	private static $connectError = '';

	/**
	* @internal
	*/
	private static $connectErrorNum = '';

	/**
	* @internal
	*/
	private static $arConfig = array();

	/**
	* @internal
	*/
	private static $arAliasToTable = array();

	/**
	* @var string $tableName имя таблицы БД
	*/
	protected $tableName = '';
	
	/**
	* @internal
	*/
	private static $encryptionFrase = '';

	/**
	* @var string $LAST_ERROR последняя ошибка sql-запроса, mysqli_stmt или самого метода
	*/
	
	public $LAST_ERROR = '';
	
	/**
	* @var boolean
	*/
	protected $DEBUG = false;
	
	protected $connConfig = [];

	/**
	* Конструктор
	*
	* @param array $config
	* @param string $tableName имя таблицы в базе
	*/
	public function __construct($config, $tableName='') {
		$this->DEBUG = $config['DEBUG'];
        $this->connConfig = $config;
        
		if (!is_object(self::$DB)) {
			self::$DB = new \mysqli($config['HOST'], $config['LOGIN'], $config['PASS'], $config['DB']);
			if (self::$DB->connect_error) {
				self::$connectErrorNum = self::$DB->connect_errno;
				self::$connectError = self::$DB->connect_error;
				\Lib\App::log('Connection error: '.self::$DB->connect_error);
			}
			else {
				self::$DB->set_charset('utf8');
				self::$arConfig = $this->addSearchTable(include(__DIR__.'/.config_orm.php'));
				
				foreach(self::$arConfig as $tName=>$arConf) {
					if (!empty($arConf['ALIAS'])) {
						self::$arAliasToTable[$arConf['ALIAS']] = $tName;
					}
				}
			}
		}

		$this->tableName = $tableName;
	}
	
	
	public function setEncryptionFrase($frase) {
		self::$encryptionFrase = strval($frase);
	}
	
	
	public static function encryptText($text, $frase) {
		$sql = 'SELECT (AES_ENCRYPT(?, UNHEX(SHA2(?,512)))) AS ENCRYPTED_VALUE';
		$stmt = self::$DB->prepare($sql);
		$stmt->bind_param('ss', $text, $frase);
		\Lib\App::log('Encryption error: '.$stmt->error);
		$stmt->execute();
		$encodedText = '';
		$stmt->bind_result($encodedText);
		\Lib\App::log('Encryption error2: '.$stmt->error);
		if ($stmt->fetch()) {
            return $encodedText;
        }
        
        return '';
	}
	
	
	public static function decryptText($text, $frase) {
		$sql = 'SELECT (AES_DECRYPT(?, UNHEX(SHA2(?,512)))) AS DECRYPTED_VALUE';
		$stmt = self::$DB->prepare($sql);
		$stmt->bind_param('ss', $text, $frase);
		$stmt->execute();
		$decodedText = '';
		$stmt->bind_result($decodedText);
		if ($stmt->fetch()) {
            return $decodedText;
        }
        
        return '';
	}
	

	/**
	* Низкоуровневый запрос в базу
	*
	* Возвращает нативные объекты модуля mysqli в зависимости
	* от типа запроса (или булевое значение)
	* @param string $SQL полностью готовый к выполнение запрос
	* @return mixed
	*/
	public function query($SQL) {
		return self::$DB->query($SQL);
	}

	/**
	* Получить запись по ее идентификатору
	*
	* __Использование:__
	* 	$id = 1111;
	* 		$obOrm = new \Lib\Model\Orm($config, 'my_table_name');
	* 		$dbRes = $obOrm->getByID($id);
	* 		if ($arr = $dbRes->fetch()) {
	* 			print_r($arr);
	* 		}
	* @param mixed $ID значение PRIMARY в таблице
	* @return \Lib\Base\Result
	*/
	public function getByID($ID) {
		$obT = self::getTable($this->tableName);
		$arTabConf = self::getTableConfig($this->tableName);
		$obT->addWhereExpression(array($arTabConf['KEY']=>$ID));
		$obT->selectFields(array('*'));
		$res = $this->doSelect($obT);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
		}

		return $res;
	}

	/**
	* Получить список записей
	*
	* 	Формат передачи полей
	* 	1. Передача с алиасом (только в селекте): 'filedName'=>'alias' или 'selfFieldName.otherFieldName'=>'alias'
	* 	2. Передача внешнего поля через привязку: 'selfFieldName.otherFieldName'
	* 	3. Передача своего поля
	* 2 формат допустим в сортировке, фильтре, группировке, селекте.
	* В селекте, фильтре допускается использование прямого sql - вставляется всегда строкой без ключа массива. Например:
	* 'now() as now_datetime', '1=1'. Поскольку выборка полей из собственной таблицы всегда идет с алиасом из конфига, то можно
	* использовать что-то вроде 'PL.DATE_CREATE > now()' - алиасы связанных таблиц меняются (добавляется счетчик) во избежание
	* конфликтов при нескольких подключениях одной и той же таблицы.
	*
	* Поля из связанных таблиц по-умолчанию возвращаются через знак подчеркивания от поля, хранящего связть с внешней таблицей.
	* Например в селекте 'selfFieldName.otherFieldName' в результате превратится в 'selfFieldName_otherFieldName'.
	*
	* __Использование:__
	*  	$arSort = array('DATE_CREATE'=>'DESC','NAME'=>'ASC');
	*  	$arFilter = array('EXT_PRIM_ID_FIELD'=>$extId, '><DATE_CREATE'=>array('2014-10-11 00:00:00', '2014-11-05 00:00:00'));
	*  	$arNavParams = array('PAGE_SIZE'=>20);
*  		$obOrm = new \Lib\Model\Orm($config, 'my_table_name');
*  		$dbRes = $obOrm->getList($arSort, $arFilter, false, $arNavParams);
*  		while ($arr = $dbRes->fetch()) {
*  			print_r($arr);
*  		}
	* Модификаторы фильтров:
	*
	* пустой или = прямое совпадение со значением
	*
	* &lt;, &lt;= - меньше, меньше или равно
	*
	* &gt;, &gt;= - больше, больше или равно
	*
	* &gt;&lt; - между, в качестве значение должен быть массив из двух значений
	*
	* !, != - не равно
	*
	* ?, !? - сравнение строк с использованием LIKE, NOT LIKE
	*
	* @param array $arOrder сортировка - массив вида имя-поля=>направление, по-умолчанию нет
	* @param array $arFilter фильтр - массив вида модификатор-имя-поля=>значение, по-умолчанию все записи
	* @param array|false $arGroupBy группировка - список полей, по-умолчанию без группировки
	* @param array|false $arNavParams параметры постраничной навигации - два варианта: если указан TOP_LIMIT, идет ограничение списка без постранички;
	* иначе, если указан PAGE_SIZE формируется постраничная навигация, реализуемая через mysqli_result::data_seek()
	* по-умолчанию без постранички
	* @param array|false $arSelect список возвращаемых полей, по-умолчанию - все поля
	* @return \Lib\Base\Result
	*/
	public function getList($arOrder=array(), $arFilter=array(), $arGroupBy=false, $arNavParams=false, $arSelect=false) {

		$obT = self::getTable($this->tableName);
		$obT->addOrderBy($arOrder);
		if (array_key_exists('SEARCH', $arFilter)) {
            $queryWords = $this->parseSearchQuery($arFilter['SEARCH']);
            $searchConf = $this->getTableConfig(self::SEARCH_INDEX_TABLE);
            $obTS = self::getTable(self::SEARCH_INDEX_TABLE);
            $expression1 = $searchConf['ALIAS'].'.LINK_TYPE = \''.$this->tableName.'\'';
            $expression2 = $searchConf['ALIAS'].'.HASHVALUE IN (MD5(\''.implode('\'),MD5(\'', $queryWords).'\'))';
            $searchFilter = [
                $expression1,
                $expression2,
            ];
            $obTS->addWhereExpression($searchFilter);
            $obTS->selectFields(['LINK_ID']);
            $subQuery = $obTS->getSelectQuery(false);
            $obT->addSearchQuery($subQuery, [$this->tableName]);
            unset($arFilter['SEARCH']);
		}
		$obT->addWhereExpression($arFilter);
		$obT->addGroupBy($arGroupBy);
		if (!is_array($arNavParams)) {
            $arNavParams = [];
		}
		if (count($arNavParams)) {
			$obT->setNavParams($arNavParams);
		}

		if (!is_array($arSelect) || !count($arSelect)) {
			$arSelect = array('*');
		}
		$obT->selectFields($arSelect);

		$bUseNav = array_key_exists('PAGE_SIZE', $arNavParams) && !array_key_exists('TOP_LIMIT', $arNavParams);
		$res = $this->doSelect($obT, $bUseNav);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
		}
		if (is_array($arNavParams) && count($arNavParams)) {
			$res->setNavParams($arNavParams);
		}

		return $res;
	}
	
	
	public function parseSearchQuery($query) {
        $query = strtolower($query);
        // удалить спецсимволы, порезать на слова
        $query = preg_replace("/[^\w\ ]+/", '', $query); // strip all punctuation characters, news lines, etc.
        $words = preg_split("/\s+/", $query, -1, PREG_SPLIT_NO_EMPTY); // split by left over spaces
        
        return $words;
	}

	/**
	* Обновление записи в таблице
	*
	* По переданному первичному ключу таблицы обновляются
	* переданные поля, из полей удаляется поле даты создания, если указано в конфиге таблицы,
	* и добавляется поле даты обновления, если также указано в конфиге. Поскольку пока
	* возвращается Result, то проверять успешность приходится через его метод ->isValid()
	*
	* __Использование:__
	* 	 $id = 1111;
	* 	 $arFields = array('ACTIVE'=>1);
* 	 	  $obOrm = new \Lib\Model\Orm($config, 'my_table_name');
* 	 	$dbRes = $obOrm->update($id, $arFields);
* 	 	if ($dbRes->isValid()) {
* 	 		print('Ok');
* 	 	}
* 	 	else {
* 	 		print('Fail');
* 	 	}
	*
	* @param mixed $ID значение первичного ключа записи в таблице БД
	* @param array $arFields массив значений вида имя_поля=>значение
	* @return \Lib\Base\Result - пока так, а вообще-то надо бы boolean возвращать
	* @todo изменить возвращаемое значение с Result на boolean
	*/
	public function update($ID, $arFields) {
		$obT = self::getTable($this->tableName);
		$arTabConf = self::getTableConfig($this->tableName);
		$obT->addWhereExpression(array($arTabConf['KEY']=>$ID));
		$obT->setValues($arFields);
		$res = $this->doUpdate($obT);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
		}
		else {
            $this->updateIndex([$ID], true);
		}

		return $res;
	}

	/**
	* Массовое обновление записей в таблице
	*
	* Позволяет по массиву первичных ключей обновить сразу несколько
	* записей, записав в их поля единый набор значений. Т.е. всем записям
	* назначаются одинаковые значения для указанных полей.
	* Из полей аналогично update удаляется дата создания и добавляется дата обновления
	*
	* __Использование:__
	* 	 $arId = array(1111, 222, 52367);
	* 	 $arFields = array('ACTIVE'=>1, 'DATE_CREATE'=>'2014-02-15 12:44:55'); // DATE_CREATE - будет проигнорировано
* 	 	$obOrm = new \Lib\Base\COrm($config, 'my_table_name');
* 	 	$dbRes = $obOrm->updateBatch($arId, $arFields, true);
* 	 	if ($dbRes->isValid()) {
* 	 		print('Ok');
* 	 	}
* 	 	else {
* 	 		print('Fail');
* 	 	}
	* @see \Lib\Base\COrm::update()
	* @param array $arId массив первичных ключей
	* @param array $arFields массив обновляемых полей вида имя_поля=>значение
	* @param boolean $confirmUpdate поскольку операция рискованная она будет выполнена только, если разработчик не забыл здесь указать true
	* @return \Lib\Base\Result - пока так, а надо бы возвращать boolean
	* @todo изменить возвращаемое значение с Result на boolean
	*/
	public function updateBatch($arId, $arFields, $confirmUpdate=false) {
		if (!$confirmUpdate || !is_array($arId) || empty($arId)) {
			return 0;
		}
		$obT = self::getTable($this->tableName);
		$arTabConf = self::getTableConfig($this->tableName);
		$obT->addWhereExpression(array($arTabConf['KEY']=>$arId));
		$obT->setValues($arFields);
		$res = $this->doUpdate($obT);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
		}
		else {
            $this->updateIndex($arId, true);
		}

		return $res;
	}

	/**
	* Добавить запись в таблицу БД
	*
	* Передается массив полей вида имя_поля=>значение. Метод запускает проверку полей
	* на валидность в соответствии с конфигом таблицы в файле .config_orm.php
	* Проверка идет через метод checkFields()
	*
	* __Использование:__
	* 	$arFields = array('NAME'=>'Some title','ACTIVE'=>0, 'DATE_CREATE'=>date('Y-m-d H:i:s')); // DATE_CREATE будет проигнорировано, хотя это спорно
* 		$obOrm = new \Lib\Base\COrm($config, 'my_table_name');
* 		$dbRes = $obOrm->add($arFields);
* 		if ($dbRes->isValid()) { // или, если автоинкрементный перв.ключ, if ($dbRes > 0)
* 			print('Ok');
* 		}
* 		else {
* 			print($obOrm->LAST_ERROR);
* 		}
	* @param array $arFields массив значений полей добавляемой в таблицу записи
	* @return mixed|\Lib\Base\Result объект Result или первичный ключ, если он является автоинкрементным
	*/
	public function add($arFields) {
		$arErrors = $this->checkFields($arFields);

		if (count($arErrors)) {
			return new Result(false, 'insert', $arErrors);
		}
		$obT = self::getTable($this->tableName);
		$obT->setValues($arFields);
		$res = $this->doInsert($obT);
		//\Lib\App::log('add values :'.print_r($arFields, true).' into: '.$this->tableName);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
			\Lib\App::log('error :'.$this->LAST_ERROR);
		}
		else {
			$arTabConf = self::getTableConfig($this->tableName);
			if (array_key_exists('KEY', $arTabConf)
				&& !empty($arTabConf['KEY'])
				&& array_key_exists('KEY_AI', $arTabConf) && !empty($arTabConf['KEY_AI'])
				&& $arTabConf['FIELDS'][$arTabConf['KEY']]['TYPE'] == 'i'
				) {

				$res->freeResult();
				$res = self::$DB->insert_id;
				
                $this->updateIndex([$res], false);
			}
			//else \Lib\App::log('no key :'.print_r($arTabConf, true));

		}

		return $res;
	}
	
	
	protected function updateIndex($ids, $clearOld=true) {
        $arTabConf = self::getTableConfig($this->tableName);
        if (!is_array($arTabConf['INDEXED'])) {
            \Lib\App::log('Not indexed: '.print_r($arTabConf, true));
            return;
        }
        $obSearchTbl = new Orm($this->connConfig, self::SEARCH_INDEX_TABLE);
        
        foreach($ids as $id) {
            $words = [];
            $fields = $this->getByID($id)->fetch();
            \Lib\App::log('Search fields: '.print_r($fields, true));
            foreach($fields as $fName => $fValue) {
                if (in_array($fName, $arTabConf['INDEXED'])) {
                    $words = array_merge($words, $this->parseSearchQuery($fValue));
                }
            }
            array_unique($words);
            \Lib\App::log('Search words: '.print_r($words, true));
            
            if ($clearOld) {
                $obSearchTbl->deleteBy(['LINK_TYPE'=>$this->tableName, 'LINK_ID'=>$id]);
            }
            if (count($words)) {
                foreach($words as $w) {
                    $add = [
                        'LINK_TYPE' => $this->tableName,
                        'LINK_ID'   => $id,
                        'HASHVALUE' => md5($w),
                    ];
                    $obSearchTbl->add($add);
                }
            }
        }
	}
	
	protected function deleteIndex($ids) {
        $arTabConf = self::getTableConfig($this->tableName);
        if (!is_array($arTabConf['INDEXED'])) return;
        $obSearchTbl = new Orm($this->connConfig, self::SEARCH_INDEX_TABLE);
        
        $obSearchTbl->deleteBy(['LINK_TYPE'=>$this->tableName, 'LINK_ID'=>$ids]);
	}
	
	protected function deleteIndexByFilter($filter) {
        $arTabConf = self::getTableConfig($this->tableName);
        if (!is_array($arTabConf['INDEXED'])) return;
        
        $obSearchTbl = self::getTable(self::SEARCH_INDEX_TABLE);
        $obT = self::getTable($this->tableName);
        $obT->selectFields([$arTabConf['KEY']]);
        $obT->addWhereExpression($filter);
        $subQuery = $obT->getSelectQuery();
        
        $obSearchTbl->addSubQuery(['=LINK_ID'=>$subQuery]);
        $obSearchTbl->addWhereExpression(['LINK_TYPE'=>$this->tableName]);
        $res = $this->doDelete($obSearchTbl);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
		}
	}

	/**
	* Удалить запись из таблицы
	*
	* Удаляет запись из таблицы по первичному ключу
	*
	* __Использование:__
	* 	$id = 1111;
* 		$obOrm = new \Lib\Base\COrm($config, 'my_table_name');
* 		if ($obOrm->delete($id)) {
* 			print('Ok');
* 		}
* 		else {
* 			print($obOrm->LAST_ERROR);
* 		}
	* @param mixed $ID значение первичного ключа записи
	* @return boolean успешность удаления
	*/
	public function delete($ID) {
		$obT = self::getTable($this->tableName);
		$arTabConf = self::getTableConfig($this->tableName);
		$obT->addWhereExpression(array($arTabConf['KEY']=>$ID));
		$res = $this->doDelete($obT);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
			return false;
		}
		$this->deleteIndex([$ID]);

		return true;
	}



	/**
	* Удалить записи из таблицы по фильтру
	*
	* Удаляет записи, подпавшие под фильтр
	*
	* __Использование:__
	* 	$arFilter = array('ID' => 1111);
* 		$obOrm = new \Lib\Model\Orm($config, 'my_table_name');
* 		if ($obOrm->deleteBy($arFilter)) {
* 			print('Ok');
* 		}
* 		else {
* 			print($obOrm->LAST_ERROR);
* 		}
	* @param array $arFilter условия для where
	* @return boolean успешность удаления
	*/
	public function deleteBy($arFilter) {
		if (empty($arFilter) || !is_array($arFilter)) {
			return false;
		}
		$obT = self::getTable($this->tableName);
		$arTabConf = self::getTableConfig($this->tableName);
		$obT->addWhereExpression($arFilter);
		// защита от случайного удаления всех записей
		// есть в самом методе удаления в $obT

		$res = $this->doDelete($obT);
		if (!$res->isValid()) {
			$this->LAST_ERROR = '['.$this->getErrorNum().']'.$this->getError().' SQL: '.$obT->lastSql;
			return false;
		}
		else {
            $this->deleteIndexByFilter($arFilter);
		}

		return true;
	}

	/**
	* Проверка корректности полей для обновления или добавления
	*
	* В настоящий момент метод используется только при добавлении записи.
	* Поля передаются по ссылке, поэтому могут менять свой состав:
	* при операции добавления в таблицу устанавливаются поля, отсутствующие
	* в $arFields, но имеющие значение DEFAULT в конфиге поля в .config_orm.php
	* @param array $arFields массив проверяемых полей
	* @param boolean $bUpdate true - идет проверка перед обновлением записи, иначе перед добавлением
	* @return boolean|array false - конфиг таблицы не найден, непустой array - массив ошибок
	*/
	public function checkFields(&$arFields, $bUpdate=false) {
		$arTabConf = self::getTableConfig($this->tableName);
		if (empty($arTabConf)) {
			return false;
		}

		$arErrors = array();

		foreach($arTabConf['FIELDS'] as $code=>$arInfo) {
			if (!array_key_exists('REQUIRED', $arInfo)) {
				$arInfo['REQUIRED'] = 0;
			}
			if ((array_key_exists($code, $arFields) && !$bUpdate) && !strlen($arFields[$code]) && array_key_exists('DEFAULT', $arInfo)) {
				$arFields[$code] = $arInfo['DEFAULT'];
			}
			if ($arInfo['REQUIRED'] && (array_key_exists($code, $arFields) || !$bUpdate) && !strlen($arFields[$code])) {
				$arErrors[] = array('ID'=>$code, 'ERROR'=>'EMPTY_REQUIRED');
			}
			if (array_key_exists('MAX_SIZE', $arInfo) && $arInfo['MAX_SIZE'] && array_key_exists($code, $arFields) && strlen($arFields[$code]) > $arInfo['MAX_SIZE']) {
				$arErrors[] = array('ID'=>$code, 'ERROR'=>'MAX_SIZE_LONGER');
			}
		}
		if (count($arErrors)) {
			\Lib\App::log('Insert errors: '.print_r($arErrors, true));
		}

		return $arErrors;
	}

	/**
	* Получить конфиг таблицы по ее алиасу
	*
	* В данной системе алиас таблицы прописывается жестко и сразу - в конфиге
	* поэтому планируется в дальнейшем использовать алиас в качестве префикса к полям
	* внешним при выборке из таблицы. Через данный метод можно будет получить всю
	* информацию о таблице такого внешнего поля
	* @param string $alias алиас таблицы
	* @return array если конфиг не найден, массив пустой
	*/
	public function getTableConfigByAlias($alias) {
		if (array_key_exists($alias, self::$arAliasToTable)) {
			return self::getTableConfig(self::aliasToTableName($alias));
		}
		return array();
	}

	public function addSearchTable($tablesConfig) {
        $tablesConfig['pn_search_index'] = [
            'ALIAS'       => 'PX',
            'ENCRYPTED'   => [],
            'INDEXED'     => [],
            'FIELDS' => [
                'LINK_TYPE' => [
                    'TITLE'    => 'Сущность',
                    'TYPE'     => 's',
                    'REQUIRED' => 1,
                    'SQL'      => 'varchar(100) not null',
                ],
                'LINK_ID' => [
                    'TITLE'    => 'Ид сущности',
                    'TYPE'     => 'i',
                    'REQUIRED' => 0,
                    'DEFAULT'  => 0,
                    'SQL'      => 'int(18) not null',
                ],
                'HASHVALUE' => [
                    'TITLE'    => 'Хеш слова',
                    'TYPE'     => 's',
                    'IS_CODE'  => 1,
                    'MAX_SIZE' => 32,
                    'REQUIRED' => 1,
                    'SQL'      => 'varchar(32) not null',
                ],
            ],

        ];
        // ALTER TABLE `pn_search_index` ADD INDEX( `LINK_TYPE`, `HASHVALUE`); 
        
        return $tablesConfig;
	}
	/**
	* Получить конфиг по имени таблицы
	*
	* Находит один или несколько конфигов по переданным именам (массивом) или одному имени таблицы
	* @param string|array $mTable имена таблиц
	* @return array массив конфига или массив нескольких массивов конфигов
	*/
	public static function getTableConfig($mTable=false) {
		if (is_string($mTable) && !empty($mTable)) {
			if (array_key_exists($mTable, self::$arConfig) || self::loadTableConfig($mTable)) {
				return self::$arConfig[$mTable];
			}
			else {
				return array();
			}
		}
		elseif(is_array($mTable) && count($mTable)) {
			//\Lib\App::log('1005');
			$arReturn=array();
			foreach($mTable as $table) {
				if (array_key_exists($table, self::$arConfig) || self::loadTableConfig($table)) {
					$arReturn[$table] = self::$arConfig[$table];
				}
				else {
					// TODO exception
					//\Lib\App::log('1005');
					return array();
				}
			}
			//\Lib\App::log('1006');
			return $arReturn;
		}
		//\Lib\App::log('1007');
		return array();
	}
	
	
	
	/**
	* Получить конфиг по имени таблицы
	*
	* Находит один или несколько конфигов по переданным именам (массивом) или одному имени таблицы
	* @param string|array $mTable имена таблиц
	* @return array массив конфига или массив нескольких массивов конфигов
	*/
	public final function getTableConfigNonStatic($mTable=false) {
		//\Lib\App::log('Table name: '.$mTable);
		//\Lib\App::log('Configs: '.print_r(self::$arConfig, true));
		//\Lib\App::log('1000');

		if (empty($mTable) && $this && get_class($this) == '\Lib\Base\COrm') {

			$mTable = $this->tableName;
		}
		
		//\Lib\App::log('1001');
		if (is_string($mTable) && !empty($mTable)) {
			//\Lib\App::log('1002');
			if (array_key_exists($mTable, self::$arConfig) || self::loadTableConfig($mTable)) {
				//\Lib\App::log('Table config: '.print_r(self::$arConfig[$mTable], true));
				//\Lib\App::log('1003');
				return self::$arConfig[$mTable];
			}
			else {
				// TODO exception
				//\Lib\App::log('1004');
				return array();
			}
		}
		elseif(is_array($mTable) && count($mTable)) {
			//\Lib\App::log('1005');
			$arReturn=array();
			foreach($mTable as $table) {
				if (array_key_exists($table, self::$arConfig) || self::loadTableConfig($table)) {
					$arReturn[$table] = self::$arConfig[$table];
				}
				else {
					// TODO exception
					//\Lib\App::log('1005');
					return array();
				}
			}
			//\Lib\App::log('1006');
			return $arReturn;
		}
		//\Lib\App::log('1007');
		return array();
	}

	/**
	* @internal
	*/
	protected final function getError() {
		return self::$DB->error;
	}

	/**
	* @internal
	*/
	protected final function getErrorNum() {
		return self::$DB->errno;
	}

	/**
	* @internal
	*/
	public static function loadTableConfig($table) {
		return array_key_exists($table, self::$arConfig);
	}

	/**
	* @internal
	*/
	public static function getTable($table) {
		$tbl = new Table($table, self::getTableConfig($table));
		$tbl->setEncryptionFrase(self::$encryptionFrase);
		return $tbl;
	}

	/**
	* @internal
	*/
	public static final function getTableByAlias($alias) {
			return new Table(self::$arAliasToTable[$alias], self::getTableConfig(self::aliasToTableName($alias)));
	}

	/**
	* @internal
	*/
	public static final function aliasToTableName($alias) {
		if (array_key_exists($alias, self::$arAliasToTable)) {
			return self::$arAliasToTable[$alias];
		}

		return '';
	}

	/**
	* @internal
	*/
	public static final function tableNameToAlias($name) {
		if (array_key_exists($name, self::$arConfig) || self::loadTableConfig($name)) {
			return self::$arConfig[$name]['ALIAS'];
		}

		return '';
	}

	/**
	* @internal
	*/
	protected final function doSelect($obTable, $bUseNav=false) {
		$result=false;
		$numRows = false;
		$SQL = $obTable->getSelectQuery();
		//if (strpos($SQL, '42') !== false) {
			//\Lib\App::log('Select SQL: '.$SQL);
		//}
		if ($obTable->isPrepared) {
			$types = $obTable->getPlacehoderTypes();
			$arData = $obTable->getPlacehoderData();
			//\Lib\App::log('Select types: '.$types);
			//\Lib\App::log('Select arData: '.print_r($arData, true));

			if ($stmt = self::$DB->prepare($SQL)) {
				array_unshift($arData, $types);
				$arRefData = pnRefValues($arData);
				//echo '<pre>'; print_r($arData); echo '</pre>';


				if ($bUseNav) {
					$navSQL = preg_replace('/select\s.+\sfrom\s/i', 'select count(*) as CNT from ', $SQL);

					$stmt2 = self::$DB->prepare($navSQL);
					$ref2    = new \ReflectionClass('\mysqli_stmt');
					$method2 = $ref2->getMethod("bind_param");
					$method2->invokeArgs($stmt2,$arRefData);
					if ($stmt2->execute()) {
						$result2 = $stmt2->get_result();
						if ($arr = $result2->fetch_assoc()) {
							$numRows = intval($arr['CNT']);
						}
						$result2->free();
					}
				}

				$ref    = new \ReflectionClass('\mysqli_stmt');
				$method = $ref->getMethod("bind_param");
				$method->invokeArgs($stmt,$arRefData);
				if ($stmt->execute()) {
					/*
					if ($stmt->store_result()) {
						\Lib\App::log('Stmt error: '.$stmt->error);
					}
					*/
					//\Lib\App::log('Stmt error: '.$stmt->error);
					$result = $stmt->get_result();
				}
				/*
				if (call_user_func_array(array($stmt, 'bind_param'), $arRefData) && $stmt->execute()) {
					$result = $stmt->get_result();
				}
				*/
			}
			else {
				\Lib\App::log('not prepared. err: '.self::$DB->error);
			}
		}
		else {
			$result = self::$DB->query($SQL);
			if (!$result) {
				\Lib\App::log('not prepared. not result err: '.self::$DB->error);
			}
		}

		$obRes = new Result($result, 'select');
		if ($numRows !== false) {
			$obRes->setNumRows($numRows);
		}
		return $obRes;
	}

	/**
	* @internal
	*/
	protected final function doUpdate($obTable) {
		$result=false;
		$SQL = $obTable->getUpdateQuery();

		//\Lib\App::log('Update SQL: '.$SQL);

		if ($obTable->isPrepared) {
			$types = $obTable->getPlacehoderTypes();
			$arData = $obTable->getPlacehoderData();
			array_unshift($arData, $types);
			$arRefData = pnRefValues($arData);

			//\Lib\App::log('Update. Data: '.print_r($arRefData, true));

			if ($stmt = self::$DB->prepare($SQL)) {

				//echo '<pre>'; print_r($arData); echo '</pre>';

				$ref    = new \ReflectionClass('\mysqli_stmt');
				$method = $ref->getMethod("bind_param");
				$method->invokeArgs($stmt,$arRefData);
				$result = $stmt->execute();
				/*
				if (call_user_func_array(array($stmt, 'bind_param'), $arRefData) && $stmt->execute()) {
					$result = $stmt->get_result();
				}
				*/
			}
			else {
				\Lib\App::log('not prepared. err: '.self::$DB->error);
				\Lib\App::log('err. SQL: '.$SQL);
				\Lib\App::log('err. Params: '.print_r($arRefData, true));
			}
		}
		else {
			$result = self::$DB->query($SQL);
		}

		$obRes = new Result($result, 'update');
		return $obRes;
	}

	/**
	* @internal
	*/
	protected final function doInsert($obTable) {
		$result=false;
		$SQL = $obTable->getInsertQuery();
		//\Lib\App::log('insert sql: '.$SQL);
		if ($obTable->isPrepared) {
			$types = $obTable->getPlacehoderTypes();
			$arData = $obTable->getPlacehoderData();

			if ($stmt = self::$DB->prepare($SQL)) {
				array_unshift($arData, $types);
				$arRefData = pnRefValues($arData);
				//echo '<pre>'; print_r($arData); echo '</pre>';
				//\Lib\App::log('insert data: '.print_r($arData, true));

				$ref    = new \ReflectionClass('\mysqli_stmt');
				$method = $ref->getMethod("bind_param");
				$method->invokeArgs($stmt,$arRefData);
				$result = $stmt->execute();
				if (!$result) {
					$this->LAST_ERROR = $stmt->error;
					\Lib\App::log('err: '.$stmt->error);
				}
				/*
				if (call_user_func_array(array($stmt, 'bind_param'), $arRefData) && $stmt->execute()) {
					$result = $stmt->get_result();
				}
				*/
			}
			else {
				\Lib\App::log('not prepared. err: '.self::$DB->error);
			}
		}
		else {
			$result = self::$DB->query($SQL);
		}

		$obRes = new Result($result, 'insert');
		return $obRes;
	}

	/**
	* @internal
	*/
	protected final function doDelete($obTable) {
		$result=false;
		$SQL = $obTable->getDeleteQuery();
		//\Lib\App::log('del sql: '.$SQL);
		if ($obTable->isPrepared) {
			$types = $obTable->getPlacehoderTypes();
			$arData = $obTable->getPlacehoderData();

			if ($stmt = self::$DB->prepare($SQL)) {
				array_unshift($arData, $types);
				$arRefData = pnRefValues($arData);
				//echo '<pre>'; print_r($arData); echo '</pre>';

				$ref    = new \ReflectionClass('\mysqli_stmt');
				$method = $ref->getMethod("bind_param");
				$method->invokeArgs($stmt,$arRefData);
				$result = $stmt->execute();
				if (!$result) {
					\Lib\App::log('err: '.$stmt->error);
				}
				//\Lib\App::log('affected: '.$stmt->affected_rows);

				/*
				if (call_user_func_array(array($stmt, 'bind_param'), $arRefData) && $stmt->execute()) {
					$result = $stmt->get_result();
				}
				*/
			}
			else {
				\Lib\App::log('not prepared. err: '.self::$DB->error);
			}
		}
		else {
			//\Lib\App::log('simple query');
			$result = self::$DB->query($SQL);
		}

		$obRes = new Result($result, 'delete');
		return $obRes;
	}



}

