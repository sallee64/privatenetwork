<?php
namespace Lib\Model;


/**
* Служебный класс для работы с результатами выборки из базы
*
* Возвращается в качестве результата при вызове методов getByID, getList, update
*/
class Result
{
	private $res=null;
	private $type = 'select';
	private $arErrors = array();
	private $arNavParams = array();
	private $numRows = 0;
	private $numPages = 0;
	private $offset = 0;
	private $offsetLimit = 0;
	private $rowCurrOffset = 0;
	private $bUseOffset = false;


	/**
	* Конструктор
	*
	* @param \mysqli_result|boolean $res
	* @param string $type тип запроса
	* @param array $arErrors ошибки во время выполнения, пока не используется
	*/
	public function __construct($res, $type='select', $arErrors=array()) {
		$this->res = $res;
		$this->arErrors = $arErrors;
	}

	/**
	* Установить кол-во всех найденных записей. Служебный метод
	*
	* Метод публичный только из-за моей кривизны в проектировании.
	* Поскольку делаются небуферизированные запросы, то без этого параметра нет
	* возможности правильно рассчитать постраничку. Поэтому методы COrm, выбирающие данные
	* делают один дополнительный запрос на получение цифры общего кол-ва результатов и
	* устанавливают полученное значение через данный метод.
	* @param integer $numRows общее кол-во записей, найденных по текущему запросу
	*/
	public function setNumRows($numRows) {
		$this->numRows = $numRows;
	}

	/**
	* Установить параметры постраничной навигации. Служебный метод
	*
	* Метод публичный только из-за моей кривизны в проектировании.
	* В CResult постраничка сработает только, если в массиве отсутствует
	* поле TOP_LIMIT, которое испльзуется Table для ограничения выборки через limit
	* Постраничка реализуется (сразу в этом методе) через вызов mysqli_result::data_seek()
	* @param array $arNavParams массив с ключами:
	* 	VAR_NAME - имя переменной, через которую передается номер текущей страницы в запросе
	* 	PAGE_SIZE - размер постранички, по-умолчанию - 100
	* 	SHOW_LIMIT - кол-во отображаемых страниц постранички. По-умолчанию - 10
	* 	PAGE - номер текущей страницы. Имеет смысл его передавать только когда указано VAR_NAME, т.е. стандартный функционал переопределен
	*/
	public function setNavParams($arNavParams) {
		static $pagerNum;

		if (intval($pagerNum) < 1) {
			$pagerNum = 0;
		}
		if ($this->type=='select' && is_array($arNavParams) && !array_key_exists('TOP_LIMIT', $arNavParams)) {

			$this->arNavParams['VAR_NAME'] = (array_key_exists('VAR_NAME', $arNavParams) && !empty($arNavParams['VAR_NAME'])) ? trim($arNavParams['VAR_NAME']) : PAGER_VAR_NAME.($pagerNum++);

			$this->arNavParams['SHOW_LIMIT'] = (array_key_exists('SHOW_LIMIT', $arNavParams) && intval($arNavParams['SHOW_LIMIT']) > 0) ? intval($arNavParams['SHOW_LIMIT']) : 10;

			$vars = $_REQUEST;
			$currPage = (array_key_exists($this->arNavParams['VAR_NAME'], $vars)) ? intval($vars[$this->arNavParams['VAR_NAME']]) : 1;

			$this->arNavParams['PAGE'] = (array_key_exists('PAGE',$arNavParams) && intval($arNavParams['PAGE']) > 0) ? intval($arNavParams['PAGE']) : $currPage;
			$this->arNavParams['PAGE_SIZE'] = (array_key_exists('PAGE_SIZE', $arNavParams) && $arNavParams['PAGE_SIZE'] > 0) ? intval($arNavParams['PAGE_SIZE']) : 100;

			//\Lib\App::log('numRows: '. $this->numRows);


			if (is_object($this->res)) {
				if ($this->numRows > 0 && $this->arNavParams['PAGE_SIZE'] > 0) {
					// pager max page
					$modDevision = $this->numRows % $this->arNavParams['PAGE_SIZE'];

					$this->numPages = ($modDevision > 0) ? intval($this->numRows/$this->arNavParams['PAGE_SIZE'])+1 : intval($this->numRows/$this->arNavParams['PAGE_SIZE']);

					// data seek
					$this->offset = ($this->arNavParams['PAGE']-1) * $this->arNavParams['PAGE_SIZE'];
					//$this->offsetLimit = ($this->arNavParams['PAGE'] == $this->numPages) ? intval($modDevision) : $this->arNavParams['PAGE_SIZE'];
					$this->offsetLimit = $this->arNavParams['PAGE_SIZE'];
					$this->bUseOffset = true;
					$this->rowCurrOffset = 0;

					$this->res->data_seek($this->offset);
				}
			}
		}
		// $this->res->data_seek(int offset)
	}

	/**
	* Получить параметры постранички
	*
	* Данный массив содержит все необходимое для шаблона, выводящего меню постраничной навигации,
	* с ключами:
	* 	все, что есть в arNavParams, а также
	* 	NUM_ROWS - кол-во всех записей
	* 	NUM_PAGES -кол-во страниц
	* 	PAGES - массив данных по каждой странице
	* 		PAGE - номер страницы
	* 		QUERY_STRING - строка для вставки в ссылку
	* 		CURRENT - boolean является ли страница текущей
	* 	PREV_PAGE_INDEX - индекс в массиве PAGES, соов. предыдущей странице
	* 	NEXT_PAGE_INDEX - индекс в массиве PAGES, соотв. следующей странице
	* 	SHOW_LIMIT - ограничение видимой области меню постранички (сколько ссылок выводить)
	* 	START_INDEX - при установленном SHOW_LIMIT индекс в PAGES, соотв. началу отображемой области
	* 	STOP_INDEX - при установленном SHOW_LIMIT индекс в PAGES, соотв. концу отображаемой области (включительно)
	* @return boolean|array false - если постраничка не используется
	*/
	public function getNavArray() {
		if (!$this->bUseOffset) {
			return false;
		}

		$arResult = $this->arNavParams;
		$arResult['NUM_ROWS'] = $this->numRows;
		$arResult['NUM_PAGES'] = $this->numPages;
		$arResult['PAGES'] = array();

		for($i=1;$i<=$this->numPages;$i++) {
			$arResult['PAGES'][] = array(
				'PAGE'=>$i,
				'QUERY_STRING'=>$this->arNavParams['VAR_NAME'].'='.$i,
				'FORM_FIELD'=>'<input type="hidden" name="'.$this->arNavParams['VAR_NAME'].'" value="'.$i.'" />',
				'CURRENT'=>($i==$this->arNavParams['PAGE']),
			);
		}

		$arResult['PREV_PAGE_INDEX'] = ($arResult['PAGE']==1) ? false : $arResult['PAGE']-2;
		$arResult['NEXT_PAGE_INDEX'] = ($arResult['PAGE']==$arResult['NUM_PAGES']) ? false : $arResult['PAGE'];
		// show limit
		if (!empty($this->arNavParams['SHOW_LIMIT'])) {
			$itemsBefore = intval($this->arNavParams['SHOW_LIMIT'] / 2);
			if ($itemsBefore > $arResult['PAGE']) {
				$itemsBefore = $arResult['PAGE'];
			}
			$itemsBefore -=1;
			$itemsAfter = $this->arNavParams['SHOW_LIMIT']-$itemsBefore;
			if (($arResult['PAGE']+$itemsAfter) > $arResult['NUM_PAGES']) {
				$itemsAfter = $arResult['NUM_PAGES']-$arResult['PAGE'];
				$itemsBefore = $this->arNavParams['SHOW_LIMIT']-($itemsAfter+1);
			}
			if ($itemsBefore > $arResult['PAGE']) {
				$itemsBefore = $arResult['PAGE']-1;
			}

			$currIndex = $arResult['PAGE']-1;
			$startIndex = $currIndex-$itemsBefore;
			if (array_key_exists($startIndex, $arResult['PAGES'])) {
				$arResult['START_INDEX'] = $startIndex;
			}
			else {
				$arResult['START_INDEX'] = 0;
			}
			$stopIndex = $arResult['START_INDEX']+$this->arNavParams['SHOW_LIMIT'];
			$maxIndex = count($arResult['PAGES'])-1;
			if ($stopIndex > $maxIndex) {
				$stopIndex = $maxIndex;
			}
			$arResult['STOP_INDEX'] = $stopIndex;
		}

		return $arResult;
	}

	/**
	* Получить значение первичного автоинкрементного ключа последней добавленной записи
	*
	* Метод add самостоятельно получает данный ключ и возвращает как результат,
	* поэтому нет необходимости самостоятельного вызова данного метода.
	* @return integer
	*/
	public function getInsertId() {
		return ($this->type == 'insert') ? intval($this->res) : 0;
	}

	/**
	* Получить массив ошибок
	* @return array
	*/
	public function errors() {
		return $this->arErrors;
	}

	/**
	* Получить следующую запись текущей выборки данных
	* Испльзование: смотрите в описании к COrm::getList()
	* @see \Lib\Base\COrm::getList()
	* @return array|boolean false - достигнут конец (с учетом постранички)
	*/
	public  function fetch() {
        $result = false;
		if (is_object($this->res)) {
			if ($this->bUseOffset) {
				if ($this->rowCurrOffset < $this->offsetLimit) {
					$this->rowCurrOffset++;
					$result = $this->res->fetch_assoc();
				}
				else {
					$result = false;
				}
			}
			else {
				$result = $this->res->fetch_assoc();
			}
		}
		if (is_array($result)) {
            $temp = [];
            if (is_numeric(key($result))) {
                foreach($result as $index => $row) {
                    foreach($row as $k => $v) {
                        if (substr($k, 0, strlen('DECRYPT_')) == 'DECRYPT_') {
                            $temp[$index][substr($k, strlen('DECRYPT_'))] = $v;
                        }
                        else {
                            $temp[$index][$k] = $v;
                        }
                    }
                }
            }
            else {
                foreach($result as $k => $v) {
                    if (substr($k, 0, strlen('DECRYPT_')) == 'DECRYPT_') {
                        $temp[substr($k, strlen('DECRYPT_'))] = $v;
                    }
                    else {
                        $temp[$k] = $v;
                    }
                }
            }
            $result = $temp;
		}
		
		return $result;
	}

	/**
	* Получен ли CResult корректный ресурс
	* @return boolean
	*/
	public function isValid() {
		if (is_object($this->res)) {
			return true;
		}
		return true && $this->res;
	}

	/**
	* Освободить используему результатом память
	*/
	public function freeResult() {
		if (is_object($this->res)) {
			$this->res->free();
		}
	}
}
