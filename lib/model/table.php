<?php
namespace Lib\Model;

/**
* @internal
*/
class Table
{
	/**
	* @internal
	*/
	private static $arLinkedAliasCounter = array();

	/**
	* @internal
	*/
	private $table = '';

	/**
	* @internal
	*/
	private $arConfig = array();

	/**
	* @internal
	*/
	private $arSelectFields=array();

	/**
	* @internal
	*/
	private $arLinkedTables=array();

	/**
	* @internal
	*/
	private $arWhereExpressions=array();

	/**
	* @internal
	*/
	private $arGroupByFields=array();

	/**
	* @internal
	*/
	private $arOrderByFields=array();

	/**
	* @internal
	*/
	private $arValues=array();

	/**
	* @internal
	*/
	private $arNavParams=array();

	/**
	* @internal
	*/
	private $placeholderTypes = '';
	
	/**
	* @internal
	*/
	private $encryptionFrase = '';

	/**
	* @internal
	*/
	private $arPlaceholderData=array();

	/**
	* @var string
	*/
	public $lastSql = '';

	/**
	* @var boolean
	*/
	public $isPrepared=false;



	public function __construct($table, $arConfig) {
		$this->table = $table;
		$this->arConfig = $arConfig;
	}


	public function getTableName() {
		return $this->table;
	}
	
	public function setEncryptionFrase($frase) {
		$this->encryptionFrase = $frase;
	}

	public function isExisted() {
		return count($this->arConfig);
	}

	public function isAI() {  // auto increment
		if (array_key_exists('KEY', $this->arConfig)
				&& !empty($this->arConfig['KEY'])
				&& array_key_exists('KEY_AI', $this->arConfig) && !empty($this->arConfig['KEY_AI'])
				&& $this->arConfig['FILEDS'][$this->arConfig['KEY']]['TYPE'] == 'i'
				) {
			return true;
		}

		return false;
	}

	public function getLinks() {
		if (array_key_exists('FOREIGNS', $this->arConfig)) {
			return $this->arConfig['FOREIGNS'];
		}
		return array();
	}

	/**
	*	Формат передачи полей
	*	1. Передача поля внешней таблицы (только если та привязана по ключу к своей): 'tableName.fieldName'
	*	2. Передача внешнего поля через привязку: 'selfFieldName.otherFieldName'
	*	3. Обычная передача перечислением собственных полей таблицы
	*/
	public function addOrderBy($arOrderBy) {
		if (!is_array($arOrderBy)) {
			return;
		}
		$arValidFields = array_keys($this->arConfig['FIELDS']);
		$arValidDirection = array('ASC','DESC');
		foreach($arOrderBy as $k=>$v) {
			if (!is_string($v) || !in_array(strtoupper($v), $arValidDirection)) {
				$v = 'ASC';
			}
			if (strpos($k, '.') !== false) {
				// 1, 2 варианты
				$arFieldParts = explode('.', $k); $first = $arFieldParts[0];
				if (in_array($first, $arValidFields)) {
					if (array_key_exists('FOREIGNS', $this->arConfig) && is_array($this->arConfig['FOREIGNS'])) {
						if (array_key_exists($first, $this->arConfig['FOREIGNS'])) {
							$linkedTable = $this->arConfig['FOREIGNS'][$first];
							if ($arFieldParts[1]) {
								$arLinkParam = array(
									'TYPE'=>'LINK',
									'SQL_TYPE'=>'LEFT JOIN',
									'FIELD'=>$first,
								);
								if (($obT = $this->linkTable($linkedTable, $arLinkParam))) {
									$obT->addOrderBy(array($arFieldParts[1]=>$v));
								}
							}
						}
					}
				}
			}
			else {
				// 3 вариант
				$this->arOrderByFields[$k] = $v;
			}
		}
	}

	public function isOrdered() {
		return count($this->arOrderByFields);
	}


	/**
	*	Формат передачи полей
	*	1. Передача sql-выражения
	*	2. Передача поля внешней таблицы (только если та привязана по ключу к своей): 'tableName.fieldName'=значение
	*	3. Передача внешнего поля через привязку: 'selfFieldName.otherFieldName'=значение
	*	4. Обычная передача - поле собственной таблицы = значение
	*/
	public function addWhereExpression($arExpression) {
		if (!is_array($arExpression)) {
			return;
		}

		$arValidFields = array_keys($this->arConfig['FIELDS']);

		foreach($arExpression as $k=>$v) {
			if (is_numeric($k) && is_string($v)) {
				// 1 вариант
				$this->arWhereExpressions[] = $v;
			}
			elseif (!is_numeric($k)) {
				if (strpos($k, '.') !== false) {
					// 2, 3 варианты
					$arFieldParts = explode('.', $k); $first = $arFieldParts[0];
					list($operator, $field) = $this->getOperationAndField($first);
					if (in_array($field, $arValidFields)) {
						// 3 вариант - selfFieldName.otherFieldName
						if (array_key_exists('FOREIGNS', $this->arConfig) && is_array($this->arConfig['FOREIGNS'])) {
							if (array_key_exists($field, $this->arConfig['FOREIGNS'])) {
								$linkedTable = $this->arConfig['FOREIGNS'][$field];
								if ($arFieldParts[1]) {
									$arLinkParam = array(
										'TYPE'=>'LINK',
										'SQL_TYPE'=>'LEFT JOIN',
										'FIELD'=>$field,
									);
									if (($obT = $this->linkTable($linkedTable, $arLinkParam))) {
										$obT->addWhereExpression(
											array(
												$operator.$arFieldParts[1]=>$v
											)
										);
										$this->isPrepared=true;
									}
								}
							}
						}
					}
				}
				else {
					// 4 вариант
					$this->arWhereExpressions[] = array($k=>$v);
					$this->isPrepared=true;
				}
			}
		}
	}

	
	
	public function addSearchQuery($subQuery) {
        if (empty($this->arConfig['KEY']) || empty($subQuery)) {
            return;
        }
        
        $this->addSubQuery(['='.$this->arConfig['KEY'] => $subQuery]);
	}
	
	
	public function addSubQuery($subQuery) {
        $key = key($subQuery);
        $query = current($subQuery);
        list($oper, $field) = $this->getOperationAndField($key);
        if (!array_key_exists($field, $this->arConfig['FIELDS'])) {
            return;
        }
        $oper = (substr($oper, 0, 1) != '!') ? ' IN ' : ' NOT IN ';
        $expression = $this->arConfig['ALIAS'].'.'.$field . $oper.'('.$query.')';
        $this->arWhereExpressions[] = $expression;
	}
	
	
	
	public function isFiltered() {
		return count($this->arWhereExpressions);
	}


	/**
	*	Формат передачи полей
	*	1. Передача поля внешней таблицы (только если та привязана по ключу к своей): 'tableName.fieldName'
	*	2. Передача внешнего поля через привязку: 'selfFieldName.otherFieldName'
	*	3. Обычная передача перечислением собственных полей таблицы
	*/
	public function addGroupBy($arGroupBy) {
		if (!is_array($arGroupBy)) {
			return;
		}

		$arValidFields = array_keys($this->arConfig['FIELDS']);
		foreach($arGroupBy as $v) {
			if (strpos($v, '.') !== false) {
				// 1, 2 варианты
				$arFieldParts = explode('.', $v); $first = $arFieldParts[0];
				if (in_array($first, $arValidFields)) {
					if (array_key_exists('FOREIGNS', $this->arConfig) && is_array($this->arConfig['FOREIGNS'])) {
						if (array_key_exists($first, $this->arConfig['FOREIGNS'])) {
							$linkedTable = $this->arConfig['FOREIGNS'][$first];
							if ($arFieldParts[1]) {
								$arLinkParam = array(
									'TYPE'=>'LINK',
									'SQL_TYPE'=>'LEFT JOIN',
									'FIELD'=>$first,
								);
								if (($obT = $this->linkTable($linkedTable, $arLinkParam))) {
									$obT->addGroupBy(array($arFieldParts[1]));
								}
							}
						}
					}
				}
			}
			else {
				// 3 вариант
				$this->arGroupByFields[] = $v;
			}
		}
	}

	public function isGroupped() {
		return count($this->arGroupByFields);
	}



	public function setNavParams($arNavParams) {
		if (is_array($arNavParams) && array_key_exists('TOP_LIMIT', $arNavParams)) {
			$this->arNavParams['TOP_LIMIT'] = (intval($arNavParams['TOP_LIMIT']) > 0) ? intval($arNavParams['TOP_LIMIT']) : '';
		}
	}


	/**
	*	Формат передачи полей
	*	1. Передача с алиасом: 'filedName'=>'alias'
	*	2. Передача полей из внешней таблицы (кроме фильтра): 'tableName'=>array(fieldName1,...)
	*	3. Передача поля внешней таблицы (только если та привязана по ключу к своей): 'tableName.fieldName'
	*	4. Передача внешнего поля через привязку: 'selfFieldName.otherFieldName'
	*	5. Обычная передача перечислением собственных полей таблицы
	*/
	public function selectFields($arFields) {
		if (!is_array($arFields)) {
			return;
		}
		$arValidFields = array_keys($this->arConfig['FIELDS']);

		// отделяем нумерованные элементы массива от именованных
		foreach($arFields as $k=>$v) {
			if (is_numeric($k)) {
				// 3, 4, 5 варианты
				if (strpos($v, '.') === false) {
					// 5 вариант - поле
					if (is_array($this->arConfig['ENCRYPTED']) && in_array($v, $this->arConfig['ENCRYPTED'])) {
                        $this->arSelectFields[] = '(AES_DECRYPT('.$this->arConfig['ALIAS'].'.'.$v.', UNHEX(SHA2(\''.$this->encryptionFrase.'\',512)))) AS DECRYPT_'.$v;
					}
					else {
                        $this->arSelectFields[] = $v;
					}
				}
				else {
					// 4 вариант поле-поле
					$arFieldParts = explode('.', $v); $first = $arFieldParts[0];
					if (in_array($first, $arValidFields)) {
						// 4 вариант - selfFieldName.otherFieldName
						if (array_key_exists('FOREIGNS', $this->arConfig) && is_array($this->arConfig['FOREIGNS'])) {
							if (array_key_exists($first, $this->arConfig['FOREIGNS'])) {
								$linkedTable = $this->arConfig['FOREIGNS'][$first];
								if ($arFieldParts[1]) {
									$arLinkParam = array(
										'TYPE'=>'LINK',
										'SQL_TYPE'=>'LEFT JOIN',
										'FIELD'=>$first,
									);
									if (($obT = $this->linkTable($linkedTable, $arLinkParam))) {
										if ($arFieldParts[1] != '*') {
											$obT->selectFields(array($arFieldParts[1]=>$first.'_'.$arFieldParts[1]));
										}
										else {
											$arLinkConf = COrm::getTableConfig($linkedTable);
											$arLF = array_keys($arLinkConf['FIELDS']);
											$arLSel = array();
											foreach($arLF as $f) {
												$arLSel[$f]=$first.'_'.$f;
											}
											$obT->selectFields($arLSel);
										}
									}
								}
							}
						}
					}
					// простой sql может содержать точки
					else {
						$this->arSelectFields[] = $v;
					}
				}
			}
			else {
				// 1, 2 варианты
				if (strpos($k, '.') === false) {
					// 1 вариант - поле-алиас
					if (!is_array($v)) {
						$this->arSelectFields[] = $k .' AS '.$v;
					}
					// 2 вариант - таблица-поля
					else {
						//
					}
				}
				else {

					// 4 вариант поле-поле с алиасом
					$arFieldParts = explode('.', $k); $first = $arFieldParts[0];
					if (in_array($first, $arValidFields)) {
						// 4 вариант - selfFieldName.otherFieldName
						if (array_key_exists('FOREIGNS', $this->arConfig) && is_array($this->arConfig['FOREIGNS'])) {
							if (array_key_exists($first, $this->arConfig['FOREIGNS'])) {
								$linkedTable = $this->arConfig['FOREIGNS'][$first];
								if ($arFieldParts[1]) {
									$arLinkParam = array(
										'TYPE'=>'LINK',
										'SQL_TYPE'=>'LEFT JOIN',
										'FIELD'=>$first,
									);
									if ($obT = $this->linkTable($linkedTable, $arLinkParam)) {
										$obT->selectFields(array($arFieldParts[1]=>$v));
									}
								}
							}
						}
					}
					// 3 вариант таблица-поле с алиасом
					else {
						//
					}
				}
			}
		}
	}


	private static function getLinkedAlias($alias, $arLinkParam) {
		$linkParamKey = md5(serialize($arLinkParam));
		$newAlias = $alias;
		if (array_key_exists($alias, self::$arLinkedAliasCounter)) {
			if (array_key_exists($linkParamKey, self::$arLinkedAliasCounter[$alias]['LINKED_PARAM'])) {
				return self::$arLinkedAliasCounter[$alias]['LINKED_PARAM'][$linkParamKey];
			}
			$lastCounter = self::$arLinkedAliasCounter[$alias]['LAST_NUM']+1;
			self::$arLinkedAliasCounter[$alias]['LINKED_PARAM'][$linkParamKey] = $alias.$lastCounter;
			self::$arLinkedAliasCounter[$alias]['LAST_NUM'] = $lastCounter;
		}
		else {
			self::$arLinkedAliasCounter[$alias] = array(
				'LINKED_PARAM'=>array(),
				'LAST_NUM'=>1,
			);
			$newAlias = $alias.'1';
			self::$arLinkedAliasCounter[$alias]['LINKED_PARAM'][$linkParamKey] = $newAlias;
		}

		return $newAlias;
	}

	/**
	* Подключить/получить внешнюю таблицу
	*
	* По имени и параметрам подключения определяется наличие подключенной таблицы
	* либо создание новой и возврат ее.
	*
	* Форма параметров. Предполагается только две формы: подключение через собственное поле,
	* ссылающееся на первичный ключ внешней таблицы; подключение внешней таблицы, ссылающейся каким-либо своим
	* полем на первичный ключ данной таблицы. Соответственно, исходя из этого, два вида типа:
	* 	TYPE - LINK|EXTERNAL
	* 	SQL_TYPE - INNER JOIN|LEFT JOIN
	* 	FIELD_NAME - для внешней - поле внешней таблицы; по своему полю - имя своего поля
	* @param string $name имя внешней таблицы
	* @param array $arLinkParam параметры подключения внешней таблицы
	* @return \Lib\Base\CTable
	*/
	private function linkTable($name, $arLinkParam) {
		$fullAlias = self::getLinkedAlias(COrm::tableNameToAlias($name), $arLinkParam);

		if (!array_key_exists($fullAlias, $this->arLinkedTables)) {
			$this->arLinkedTables[$fullAlias] = array(
				'TABLE'=>COrm::getTable($name),
				'PARAM'=>$arLinkParam,
				'NAME'=>$name,
			);
		}
		return $this->arLinkedTables[$fullAlias]['TABLE'];
	}


	/**
	* Есть ли связанные таблицы
	* @return integer кол-во связей
	*/
	public function hasLinked() {
		return count($this->arLinkedTables);
	}

	/**
	* Установить новые значения полей
	* @param array $arFields поля в формате имя=>значение
	*/
	public function setValues($arFields) {
		if (!is_array($arFields)) {
			return;
		}
		$this->arValues = $arFields;
		$this->isPrepared=true;
	}




	/**
	* Получить SQL запрос выборки из базы
	* @return string
	*/
	public function getSelectQuery() {
		$sql = '';
		$this->arPlaceholderData = array();
		$this->placeholderTypes = '';

		$arValidFields = array_keys($this->arConfig['FIELDS']);
		if (!$this->hasLinked()) {
			$fields=$this->getSelectFields();

			$sql = 'SELECT '.$fields.' FROM `'. $this->table .'` '.$this->arConfig['ALIAS'];

			if ($this->isFiltered()) {
				$where = $this->getWhereSql(true);
				if (strlen($where)) {
					$sql .= ' WHERE '.$where;
				}
			}
			if ($this->isGroupped()) {
				$group = $this->getGroupSql();
				if (strlen($group)) {
					$sql .= ' GROUP BY '.$group;
				}
			}
			if ($this->isOrdered()) {
				$order = $this->getOrderSql();
				if (strlen($order)) {
					$sql .= ' ORDER BY '.$order;
				}
			}
			if (count($this->arNavParams) && array_key_exists('TOP_LIMIT', $this->arNavParams)) {
				$sql .= ' LIMIT '.$this->arNavParams['TOP_LIMIT'];
			}
		}
		else {
			$fields=$this->getSelectFields();
			foreach($this->arLinkedTables as $fullAlias => $arLinkData) {
				$linkedFields = $arLinkData['TABLE']->getSelectFields($fullAlias);
				if (strlen($linkedFields)) {
					$fields .= ', '.$linkedFields;
				}
			}

			$sql = 'SELECT '.$fields.' FROM `'. $this->table .'` '.$this->arConfig['ALIAS'];

			$tables = '';
			foreach($this->arLinkedTables as $fullAlias => $arLinkData) {
				$arLinkParam = $arLinkData['PARAM'];
				$tables .= ' '.$arLinkParam['SQL_TYPE'].' `'.$arLinkData['NAME'].'` '.$fullAlias;
				if ($arLinkParam['TYPE'] == 'LINK') {
					$tables .= ' on '.$this->arConfig['ALIAS'].'.'.$arLinkParam['FIELD'].'='.$arLinkData['TABLE']->getKeyField($fullAlias);
				}
			}
			if (strlen($tables)) {
				$sql .= $tables;
			}

			$where='';
			$and = '';
			if ($this->isFiltered()) {
				$where = $this->getWhereSql(true);
				if (strlen($where)) {
					$and = 'AND';
				}
			}

			foreach($this->arLinkedTables as $fullAlias => $arLinkData) {
				$wherePart = $arLinkData['TABLE']->getWhereSql(true, $fullAlias);
				if (strlen($wherePart)) {
					$where .= ' '.$and.' '.$wherePart;
					$and = 'AND';
				}
			}
			if (strlen($where)) {
				$sql .= ' WHERE '.$where;
			}

			$group = '';
			$comma = '';
			$grp = $this->getGroupSql();
			if (strlen($grp)) {
				$group = $grp;
				$comma = ',';
			}
			foreach($this->arLinkedTables as $fullAlias => $arLinkData) {
				$grpPart = $arLinkData['TABLE']->getGroupSql($fullAlias);
				if (strlen($grpPart)) {
					$group .= $comma.' '.$grpPart;
					$comma = ',';
				}
			}
			if (strlen($group)) {
				$sql .= ' GROUP BY '.$group;
			}

			$order = '';
			$comma = '';
			$ord = $this->getOrderSql();
			if (strlen($ord)) {
				$order = $ord;
				$comma = ',';
			}
			foreach($this->arLinkedTables as $fullAlias => $arLinkData) {
				$ord = $arLinkData['TABLE']->getOrderSql($fullAlias);
				if (strlen($ord)) {
					$order .= $comma.' '.$ord;
					$comma = ',';
				}
			}
			if (strlen($order)) {
				$sql .= ' ORDER BY '.$order;
			}

			if (count($this->arNavParams) && array_key_exists('TOP_LIMIT', $this->arNavParams)) {
				$sql .= ' LIMIT '.$this->arNavParams['TOP_LIMIT'];
			}

		}

		$this->lastSql = $sql;
		
		\Lib\App::log('Select Sql: '.$sql);
		
		return $sql;
	}


	/**
	* Получить SQL-запрос обновления записи
	* @return string
	*/
	public function getUpdateQuery() {
		if (!$this->isFiltered() || !count($this->arValues)) {
			return false;
		}
		$NOW = date('Y-m-d H:i:s');
		if (array_key_exists('DATE_CREATE', $this->arConfig) && array_key_exists($this->arConfig['DATE_CREATE'], $this->arValues)) {
			unset($this->arValues[$this->arConfig['DATE_CREATE']]);
		}
		if (array_key_exists('DATE_UPDATE', $this->arConfig) && array_key_exists($this->arConfig['DATE_UPDATE'], $this->arConfig['FIELDS'])) {
			$this->arValues[$this->arConfig['DATE_UPDATE']] = $NOW;
		}
		$arValidFields = array_keys($this->arConfig['FIELDS']);
		$this->placeholderTypes = '';
		$this->arPlaceholderData = array();

		$sql = 'UPDATE `'. $this->table .'` SET ';
		$comma = '';
		foreach($this->arValues as $fieldName=>$value) {
			if (in_array($fieldName, $arValidFields)) {
				$type = $this->arConfig['FIELDS'][$fieldName]['TYPE'];
				if (is_array($this->arConfig['ENCRYPTED']) && in_array($fieldName, $this->arConfig['ENCRYPTED'])) {
                    $sql .= $comma.' `'.$fieldName.'`=AES_ENCRYPT(?, UNHEX(SHA2(\''.$this->encryptionFrase.'\',512)))';
				}
				else {
                    $sql .= $comma.' `'.$fieldName.'`=?';
				}
				$this->placeholderTypes .= $type;
				$this->arPlaceholderData[] = $value;
				$comma = ',';
			}
		}
		if ($comma != ',') {
			return false;
		}

		$where=$this->getWhereSql(false);
		if (strlen($where)) {
			$sql .= ' WHERE '.$where;
		}
		else {
			return false;
		}

		$this->lastSql = $sql;
		
		\Lib\App::log('Update Sql: '.$sql);
		
		return $sql;
	}


	/**
	* Получить SQL-запрос для вставки записи
	* @return string
	*/
	public function getInsertQuery() {
		if (!count($this->arValues)) {
			return false;
		}
		$NOW = date('Y-m-d H:i:s');
		if (array_key_exists('DATE_CREATE', $this->arConfig) && array_key_exists($this->arConfig['DATE_CREATE'], $this->arConfig['FIELDS'])) {
			$this->arValues[$this->arConfig['DATE_CREATE']] = $NOW;
		}
		if (array_key_exists('DATE_UPDATE', $this->arConfig) && array_key_exists($this->arConfig['DATE_UPDATE'], $this->arConfig['FIELDS'])) {
			$this->arValues[$this->arConfig['DATE_UPDATE']] = $NOW;
		}
		$arValidFields = array_keys($this->arConfig['FIELDS']);
		$this->placeholderTypes = '';
		$this->arPlaceholderData = array();

		$sql = 'INSERT INTO `'. $this->table .'` (';
		$values = '';
		$comma = '';
		foreach($this->arValues as $fieldName=>$value) {
			if (in_array($fieldName, $arValidFields)) {
				$type = $this->arConfig['FIELDS'][$fieldName]['TYPE'];
				$sql .= $comma.' `'.$fieldName.'`';
				if (is_array($this->arConfig['ENCRYPTED']) && in_array($fieldName, $this->arConfig['ENCRYPTED'])) {
                    $values .= $comma.'AES_ENCRYPT(?, UNHEX(SHA2(\''.$this->encryptionFrase.'\',512)))';
				}
				else {
                    $values .= $comma.'?';
				}
				$this->placeholderTypes .= $type;
				$this->arPlaceholderData[] = $value;
				$comma = ',';
			}
		}
		if ($comma != ',') {
			return false;
		}
		$sql .= ') VALUES ('.$values.')';

		$this->lastSql = $sql;
		
		\Lib\App::log('Insert Sql: '.$sql);
		
		return $sql;
	}


	/**
	* Получить SQL-запрос на удаление записи
	* @return string
	*/
	public function getDeleteQuery() {
		if (!$this->isFiltered()) {
			return false;
		}
		$arValidFields = array_keys($this->arConfig['FIELDS']);
		$this->placeholderTypes = '';
		$this->arPlaceholderData = array();

		$sql = 'DELETE FROM `'. $this->table .'` ';

		$where=$this->getWhereSql(false);
		if (strlen($where)) {
			$sql .= ' WHERE '.$where;
		}
		else {
			return false;
		}

		$this->lastSql = $sql;
		return $sql;
	}

	/**
	* @internal
	*/
	public function getSelectFields($useAlias='') {
		$bWasAlias = !empty($useAlias);
		if (empty($useAlias)) {
			$useAlias = $this->arConfig['ALIAS'];
		}
		//\Lib\App::log('Selected fields: '.print_r($this->arSelectFields, true));
		$fields='';
		if (count($this->arSelectFields)) {
			$arValidFields = array_keys($this->arConfig['FIELDS']);
			$arFields=array();
			foreach($this->arSelectFields as $fieldName) {
				if ($fieldName == '*') {
                    if (is_array($this->arConfig['ENCRYPTED']) && count($this->arConfig['ENCRYPTED'])) {
                        foreach($arValidFields as $f) {
                            if (in_array($f, $this->arConfig['ENCRYPTED'])) {
                                $arFields[] = '(AES_DECRYPT('.$this->arConfig['ALIAS'].'.'.$f.', UNHEX(SHA2(\''.$this->encryptionFrase.'\',512)))) AS DECRYPT_'.$f;
                            }
                            else {
                                $arFields[] = $useAlias.'.'.$f;
                            }
                        }
                    }
                    else {
                        if (!$bWasAlias) {
                            $arFields[] = $useAlias.'.*';
                        }
					}
					continue;
				}
				$testField = $fieldName;
				if (strpos($fieldName, ' AS ') !== false) {
					$testField = current(explode(' ', $fieldName));
				}
				if (in_array($testField, $arValidFields)) {
					$arFields[] = $useAlias.'.'.$fieldName;
				}
				else {
					// something like sql
					$arFields[] = $fieldName;
				}
			}
			if (count($arFields)) {
				$fields .= implode(',', $arFields);
			}
		}

		return $fields;
	}

	/**
	* @internal
	*/
	public function getKeyField($useAlias='') {
		if (empty($useAlias)) {
			$useAlias = $this->arConfig['ALIAS'];
		}

		return $useAlias.'.'.$this->arConfig['KEY'];
	}

	/**
	* @internal
	*/
	public function getPlacehoderData() {
		$arData = $this->arPlaceholderData;
		if ($this->hasLinked()) {
			foreach($this->arLinkedTables as $fullAlias=>$arLinkData) {
				$d = $arLinkData['TABLE']->getPlacehoderData();
				if (is_array($d)) {
					$arData +=$d;
				}
			}
		}

		return $arData;
	}

	/**
	* @internal
	*/
	public function getPlacehoderTypes() {
		$types = $this->placeholderTypes;
		if ($this->hasLinked()) {
			$comma='';
			foreach($this->arLinkedTables as $fullAlias=>$arLinkData) {
				$t = $arLinkData['TABLE']->getPlacehoderTypes();
				if (strlen($t)) {
					$types .= $comma.$t;
					$comma = ',';
				}
			}
		}

		return $types;
	}

	/**
	* @internal
	*/
	protected function getOperationAndField($fieldName) {
		$arValidOperations = array('><','>','<','>=','<=','=','!=','!','?','!?');
		// default values
		$field = $fieldName;
		$oper = '';

		$operator = substr($fieldName, 0, 2);
		if (in_array($operator, $arValidOperations)) {
			$field = substr($fieldName, 2);
			$oper = $operator;
		}
		else {
			$operator = substr($operator, 0, 1);
			if (in_array($operator, $arValidOperations)) {
				$field = substr($fieldName, 1);
				$oper = $operator;
			}
		}

		return array($oper, $field);
	}

	/**
	* @internal
	*/
	protected function getWhereOperation($expression, $bUseTableAlias=true, $useAlias='') {

		$arValidFields = array_keys($this->arConfig['FIELDS']);
		$operator = '';
		$field = '';
		$sql = '';
		if (is_string($expression)) {
			return $expression;
		}
		if (empty($useAlias)) {
			$useAlias = $this->arConfig['ALIAS'];
		}

		if (is_array($expression) && count($expression)) {

			$fieldName = key($expression);
			$value = current($expression);
			if (is_array($value) && !count($value)) {
				$value = '';
			}

			list($operator, $field) = $this->getOperationAndField($fieldName);

			$tableAlias = ($bUseTableAlias) ? $useAlias.'.' : '';
			//\Lib\App::log('where field name: '.$field);
			if (in_array($field, $arValidFields)) {
				$type = $this->arConfig['FIELDS'][$field]['TYPE'];
				switch($operator) {
					case '':
					case '=':
					case '!':
					case '!=':
						if (is_array($value)) {
							$oper = ($operator == '!' || $operator=='!=') ? 'NOT IN' : 'IN';
							$sql = $tableAlias . $field.' '.$oper.' (';
							$comma = '';
							foreach($value as $v) {
								$this->placeholderTypes .= $type;
								$this->arPlaceholderData[] = $v;
								$sql .= $comma.'?';
								$comma = ',';
							}

							$sql .= ')';
						}
						else {
							$oper = ($operator == '!' || $operator=='!=') ? '!=' : '=';
							$sql .= $tableAlias . $field.' '.$oper.'?';
							$this->placeholderTypes .= $type;
							$this->arPlaceholderData[] = $value;
						}
					break;
					case '<':
					case '>':
					case '<=':
					case '>=':
						if (is_array($value)) {
							$value = current($value);
						}
						$sql = $tableAlias . $field.$operator.'?';
						$this->placeholderTypes .= $type;
						$this->arPlaceholderData[] = $value;
					break;
					case '><':
						if (is_array($value) && count($value)>1) {
							$sql = $tableAlias . $field.' BETWEEN ? AND ?';
							for($i=0,$l=2;$i<$l;$i++) {
								$this->placeholderTypes .= $type;
								$this->arPlaceholderData[] = next($value);
							}
						}
					break;
					case '!?':
					case '?':
						if ($type == 's' && is_string($value)) {
							$oper = ($operator == '!?') ? 'NOT LIKE' : 'LIKE';
							$sql = $tableAlias . $field.' '.$oper.' ?';
							$this->placeholderTypes .= $type;
							$this->arPlaceholderData[] = $value;
						}
					break;
				}
			}
		}

		return $sql;
	}

	/**
	* @internal
	*/
	public function getWhereSql($bUseTableAlias=true, $useAlias='') {
		$where='';
		$and = '';
		foreach($this->arWhereExpressions as $exp) {
			$sqlPart = $this->getWhereOperation($exp, $bUseTableAlias, $useAlias);
			if (strlen($sqlPart)) {
				$where .= ' '.$and.' '.$sqlPart;
				$and = 'AND';
			}
		}

		return $where;
	}

	/**
	* @internal
	*/
	public function getOrderSql($useAlias='') {
		if (empty($useAlias)) {
			$useAlias = $this->arConfig['ALIAS'];
		}
		$order = '';
		$comma = '';
		foreach($this->arOrderByFields as $ord=>$direction) {
			$order .= $comma.' '.$useAlias.'.'.$ord . ' '. $direction;
			$comma = ',';
		}

		return $order;
	}

	/**
	* @internal
	*/
	public function getGroupSql($useAlias='') {
		if (empty($useAlias)) {
			$useAlias = $this->arConfig['ALIAS'];
		}
		$group = '';
		$comma = '';
		foreach($this->arGroupByFields as $f) {
			$group .= $comma.' '.$useAlias.'.'.$f;
			$comma = ',';
		}

		return $group;
	}
}
