<?php
/*
* table name pn_common_intex is used by the system to store search indexes
*/ 
return [
	// table name => (key, title, active, date_create, date_update, foreign, fields => name => (type,title,maxsize,regexp,required,default)
	'pn_settigns' => [
		'ALIAS'       => 'PS',
		'KEY'         => 'ID',
		'KEY_AI'      => 1,
		'TITLE'       => '',
		'ACTIVE'      => '',
		'DATE_CREATE' => '',
		'DATE_UPDATE' => '',
		'ENCRYPTED'   => ['VALUE',],
		'INDEXED'     => ['VALUE',],
		'FIELDS' => [
			'ID' => [
				'TITLE' => 'ID',
				'TYPE'  => 'i',
			],
			'ENTITY' => [
				'TITLE'    => 'Сущность',
				'TYPE'     => 's',
				'REQUIRED' => 1,
				'SQL'      => 'varchar(32) not null',
			],
			'ENTITY_ID' => [
				'TITLE'    => 'Ид сущности',
				'TYPE'     => 'i',
				'REQUIRED' => 0,
				'DEFAULT'  => 0,
				'SQL'      => 'int(18) not null',
			],
			'CODE' => [
				'TITLE'    => 'Код опции',
				'TYPE'     => 's',
				'IS_CODE'  => 1,
				'MAX_SIZE' => 127,
				'REQUIRED' => 1,
				'SQL'      => 'varchar(32) not null',
			],
			'VALUE' => [
				'TITLE'    => 'Значение опции',
				'TYPE'     => 's',
				'IS_CODE'  => 0,
				'REQUIRED' => 0,
				'SQL'      => 'blob',
			],
		],

	],
	
	
];
