<?php
namespace Lib\Model;

class Base
{
    protected static $DB;
    protected static $tableName;
    
    
    
    protected function __construct() {
        // static class
    }
    
    public static function getFields() {
    
    }
    
    public static function getList($params=[]) {
    
    }
    
    public static function getItem($id) {
    
    }
    
    public static function add($fields) {
    
    }
    
    public static function update($id, $fields) {
    
    }
    
    
    protected static function init() {
        if (!is_object(self::$DB)) {
			$config = \Lib\App::getConfig();
			self::$DB = new \mysqli($config['HOST'], $config['LOGIN'], $config['PASS'], $config['DB']);
			if (self::$DB->connect_error) {
				self::$connectErrorNum = self::$DB->connect_errno;
				self::$connectError = self::$DB->connect_error;
			}
			else {
				self::$DB->set_charset('utf8');
			}
		}
    }
    
}
