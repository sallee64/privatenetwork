<?php
namespace Lib;

require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include.php');

class Data
{ 
    protected $action = '';
    protected $params = [];
    
    public function run() {
        $this->prepareParams();
        $this->executeAction();
    }
    
    protected function prepareParams() {
        
    }
    
    protected function executeAction() {
        
    }
    
    protected function display($data=[]) {
        
    }
} 
