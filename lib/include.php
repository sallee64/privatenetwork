<?php

require_once(__DIR__.'/app.php');
if (function_exists('spl_autoload_register')) {
	spl_autoload_register(['\Lib\App', 'autoloadClass']);
}
else {
	die("no spl autoload");
}



/**
* Используется для преобразования копированного массива
* в массив ссылок на значения. Требуется для передачи
* параметров по ссылке в методы php-библиотеки mysqli
* @param array массив значений
* @return array массив ссылок на значения
*/
function pnRefValues($arr){
	$refs = array();
	if (!is_array($arr)) {
		return $arr;
	}

	foreach($arr as $key => $value) {
		$refs[$key] = &$arr[$key];
	}
	return $refs;
}
