<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/lib/include.php');

$app = new Lib\App;
//$data = $app->getMainVueData();
//$settings = $data['SETTINGS'];
$config = Lib\App::getConfig();
Lib\App::log('Config: '.print_r($config, true));
$sett = new Lib\Model\Orm($config, 'pn_settigns');
$secret = 'My very secret frase';
/*
$text = file_get_contents('recieved_encoded.txt');
Lib\App::log('Text: '.$text);
$decoded = Lib\Model\Orm::decryptText($text, $secret);
Lib\App::log('decoded: '.$decoded);
file_put_contents('decoded.txt', $decoded);
*/
/*
$sett->setEncryptionFrase($secret);
$res = $sett->getList([], ['SEARCH'=>'Ivan']);
$row = $res->fetch();
Lib\App::log('Row: '.print_r($row, true));
$fields = [
    'ENTITY' => 'NEWS',
    'ENTITY_ID' => '133',
    'CODE' => 'NAME',
    'VALUE' => 'Ivan Opal',
];
if ($id = $sett->add($fields)) {
    $res = $sett->getByID($id);
    $row = $res->fetch();
    Lib\App::log('Row: '.print_r($row, true));
}
*/
//$sql = "insert into `pn_settigns` (ENTITY, ENTITY_ID, CODE, VALUE) VALUES ('NEWS', 123, 'NAME', AES_ENCRYPT('Vasiliy Pechkin', UNHEX(SHA2('".$secret."',512))))";
//$res = $sett->query($sql);
//$sql = "select * from `pn_settigns` where `CODE`='NAME' and `VALUE` like AES_ENCRYPT('Vas', UNHEX(SHA2('".$secret."',512)))";
//$res = $sett->query($sql);
/*
Lib\App::log('Insert res: '.print_r($res, true));
Lib\App::log('Insert err: '.print_r($sett->LAST_ERROR, true));
$fields = [
    'ENTITY' => 'NEWS',
    'ENTITY_ID' => '77',
    'CODE' => 'ORDER',
    'VALUE' => 'DESC',
];
$res = $sett->update(2, $fields);
Lib\App::log('Update res: '.print_r($res, true));
$res = $sett->getByID(2);
$row = $res->fetch();
Lib\App::log('Row: '.print_r($row, true));
if ($id = $sett->add($fields)) {
    $res = $sett->getByID($id);
    $row = $res->fetch();
    Lib\App::log('Row: '.print_r($row, true));
}
$res = $sett->getList();
while ($row = $res->fetch()) {
    Lib\App::log('Row: '.print_r($row, true));
}
*/

$avatar = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/avatar.jpg');
$avatarMan = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/man.jpg');
$avatarWoman = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/woman.jpg');
$testImage = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/fire.jpg');
?>
<html>
    <head>
        <title>Private network</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/main.css" rel="stylesheet">
        <script src="js/main.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
    </head>
    <body>
        <div id="app">
            <div class="container">
                <header>
                    <div class="search-field"><input type="text" name="search" value=""></div>
                    <div><span id="logo">F&C</span> your private social network</div>
                </header>
                <div v-bind:class="currentLayoutClass">
                    <pages>
                        <div v-for="(item, index) in pages">
                        <?php // START PAGES % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %?>
                        
                            <div v-if="index == currentPage">
                            
                                <h2 class="page-title">{{ item.title }}</h2>
                                <p class="page-subtitle">{{ item.subtitle }}</p>
                                
                                <div v-for="(item, index) in item.data.content">
                                    <div v-if="item.type == 'personal'" class="main-name">
                                        <div v-for="(item, index) in currentUser.personal">
                                            <div v-if="item.show" class="person-item">
                                                <span class="personal-name">{{ item.title }}: <span class="personal-value">{{ item.value }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="news-item" v-if="item.type == 'post'">
                                        <div>
                                            
                                            <span class="post-date">{{ item.date }}</span>
                                            <span class="post-author">{{ users['u'+item.user.id].name }}</span>
                                            <span class="post-avatar">
                                                <img v-bind:src="'data:image/'+users['u'+item.user.id].avatar.type+';base64, '+users['u'+item.user.id].avatar.data">                                          
                                            </span>
                                        </div>
                                        <div>
                                            {{ item.body }}
                                        </div>
                                        <div v-if="item.images" class="images-box">
                                            <div v-for="(item, index) in item.images">
                                                <img v-bind:src="'data:image/'+item.type+';base64, '+item.data" v-bind:title="item.title">
                                            </div>
                                        </div>
                                        <div class="comments" v-if="item.comments">
                                            <div v-for="(item, index) in item.comments">
                                                <div class="comment-item">
                                                    <div>
                                                        <span class="comment-date">{{ item.date }}</span>
                                                        <span class="comment-author">{{ users['u'+item.user.id].name }}</span>
                                                        <span class="comment-avatar">
                                                            <img v-bind:src="'data:image/'+users['u'+item.user.id].avatar.type+';base64, '+users['u'+item.user.id].avatar.data">                                          
                                                        </span>
                                                    </div>
                                                    <div>{{ item.data }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php // END PAGES % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %?>
                        </div>
                    </pages>
                    <div v-if="currentLayoutClass == 'layout-double'" class="side-panel">
                        <div v-for="(item, index) in pages[currentPage].data.alts">
                        
                            <div v-if="item.type == 'settings'">
                                <div class="settings-block">
                                    <h4>{{ item.title }}</h4>
                                    <div v-for="(item, index) in item.fields">
                                        <div v-if="item.type == 'text'">
                                            <lable>{{ item.label }}
                                                <input type="text" v-model="item.value">
                                            </lable>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div v-if="item.type == 'avatar'" class="main-avatar">
                                <img v-bind:src="'data:image/'+settings.user.avatar.type+';base64, '+settings.user.avatar.data" v-bind:title="settings.user.avatar.title">
                            </div>
                            <div v-if="item.type == 'personal'" class="main-name">
                                <p>{{ currentUser.name }}<br>
                                    <span v-if="currentUser.status" class="main-user-status">
                                        {{ settings.user.status }}
                                    </span>
                                </p>
                                <div v-for="(item, index) in currentUser.personal">
                                    <div v-if="item.show"  class="person-item">
                                        <p><span class="name">{{ item.title }}: <span class="value">{{ item.value }}</span></p>
                                    </div>
                                </div>
                            </div>
                            <nav v-if="item.type == 'nav'">
                                <div v-for="(item, index) in pages">
                                    <div v-if="item.nav"  class="nav-item">
                                        <a href="javascript:void(0)" v-bind:class="getPageClass(index)" v-on:click="showPage(index)">{{ item.title }}</a>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <footer>
                    <p>(c) All right</p>
                </footer>
            </div>
            
            <div v-if="popups">
                <div v-for="(item, index) in popups">
                    <div class="popup-wrapper" v-on:click="closePopup(index)">
                        <div class="popup-item">
                            <span class="popup-close" v-on:click="closePopup(index)">x</span>
                            <h3>{{ item.title }}</h3>
                            <div v-bind:class="item.layout">
                                <div v-if="item.type == 'settings'">
                                    <div v-for="(item, index) in item.data.content" class="popup-content">
                                        <div class="settings-block">
                                            <h4>{{ item.title }}</h4>
                                            <div v-for="(item, index) in item.fields">
                                                <div v-if="item.type == 'text'">
                                                    <lable>{{ item.label }}
                                                        <input type="text" v-model="item.value">
                                                    </lable>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div v-if="item.layout == 'layout-double'" class="popup-blocks">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>
    <script>
        window.app = new Vue({
            el: '#app',
            data: {
                settings: {
                    key: 'sdfsfsf',
                    default_page: '01-authkey',
                    main_page: '02-main',
                    user: {
                        avatar: {
                            type: 'jpg',
                            data: '<?php echo base64_encode($avatar)?>',
                            title: '',
                        },
                        name: 'Вадим Кораблев',
                        status: 'Вы нам только шепните, мы на помощь придем. Чип и Дейл.',
                        personal: {
                            birthday: {
                                show: true,
                                title: 'День рождения',
                                value: '28 aug 1964',
                            },
                            city: {
                                show: true,
                                title: 'Город',
                                value: 'Los Angeles',
                            },
                        },
                    },
                },
                currentUser: {
                    avatar: {
                        type: 'jpg',
                        data: '<?php echo base64_encode($avatar)?>',
                        title: '',
                    },
                    name: 'Вадим Кораблев',
                    status: 'Вы нам только шепните, мы на помощь придем. Чип и Дейл.',
                    personal: {
                        birthday: {
                            show: true,
                            title: 'День рождения',
                            value: '28 aug 1964',
                        },
                        city: {
                            show: true,
                            title: 'Город',
                            value: 'Los Angeles',
                        },
                    },
                },
                popups: {
                    
                },
                users: {
                    'u1': {
                        id: 1,
                        name: 'Вадим Кораблев',
                        avatar: {
                            type: 'jpg',
                            data: '<?php echo base64_encode($avatar)?>',
                            title: '',
                        },
                    },
                    'u2': {
                        id: 2,
                        name: 'Борис',
                        avatar: {
                            type: 'jpg',
                            data: '<?php echo base64_encode($avatarMan)?>',
                            title: '',
                        },
                    },
                    'u3': {
                        id: 3,
                        name: 'Sasha Kim',
                        avatar: {
                            type: 'jpg',
                            data: '<?php echo base64_encode($avatarWoman)?>',
                            title: '',
                        },
                    },
                },
                pages: {
                    loader: {
                        nav: false,
                        layout: 'layout-single',
                        title: 'Loading...',
                        data: {
                            content: [],
                        },
                    },
                    '01-authkey': {
                        nav: false,
                        layout: 'layout-single',
                        title: 'Enter your encription key',
                        data: {
                            content: [],
                        },
                    },
                    '02-main': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Вадим Кораблев',
                        subtitle: 'Вы нам только шепните, мы на помощь придем. Чип и Дейл.',
                        data: {
                            alts: [
                                {
                                    type: 'avatar',
                                },
                                {
                                    type: 'nav',
                                },
                            ],
                            popups: {
                                mainSettings: {
                                    title: 'Основные настройки',
                                    layout: 'layout-single',
                                    type: 'settings',
                                    data: {
                                        content: [
                                            {
                                                title: 'Персональные данные',
                                                type: 'settings',
                                                fields: [
                                                    {
                                                        name: 'name',
                                                        label: 'Имя',
                                                        type: 'text',
                                                        value: 'Вадим Кораблев',
                                                    },
                                                ],
                                            }
                                        ],
                                        alts: [
                                            
                                        ],
                                    },
                                },
                            },
                            content: [
                                {
                                    type: 'personal',
                                },
                                {
                                    type: 'post',
                                    date: '12 oct 20',
                                    user: {
                                        id: '1',
                                    },
                                    body: 'Привет, парни, как вы там?',
                                    images: [
                                        {
                                            type: 'jpg',
                                            data: '<? echo base64_encode($testImage)?>',
                                            title: '',
                                        },
                                        {
                                            type: 'jpg',
                                            data: '<? echo base64_encode($testImage)?>',
                                            title: '',
                                        },
                                    ],
                                    link: {
                                        title: '',
                                        href: '',
                                        picture: '',
                                    },
                                    viewed: {
                                    
                                    },
                                    liked: {
                                    
                                    },
                                    comments: [
                                        {
                                            data: 'very good',
                                            date: '18 aug 2020',
                                            user: {
                                                name: 'Boris',
                                                id: '2',
                                            },
                                            liked: [
                                                {
                                                    user: {
                                                        name: 'Boris',
                                                        id: '1',
                                                    }
                                                }
                                            ],
                                        },
                                        {
                                            data: 'how are you?',
                                            date: '20 aug 2020',
                                            user: {
                                                name: 'Sasha Kim',
                                                id: '3',
                                            },
                                            liked: [
                                                {
                                                    user: {
                                                        name: 'Boris',
                                                        id: '1',
                                                    }
                                                }
                                            ],
                                        },
                                    ],
                                },
                                {
                                    type: 'post',
                                    date: '1 oct 20',
                                    user: {
                                        name: 'Vadim Korolev',
                                        id: '1',
                                    },
                                    body: 'Some more posts',
                                    images: [
                                        
                                    ],
                                    link: {
                                        title: '',
                                        href: '',
                                        picture: '',
                                    },
                                    viewed: {
                                    
                                    },
                                    liked: {
                                    
                                    },
                                    comments: [
                                        
                                    ],
                                },
                            ],
                        },
                    },
                    '03-news': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Новости',
                        
                        data: {
                            alts: [
                                {
                                    title: 'Персональные данные',
                                    type: 'settings',
                                    fields: [
                                        {
                                            name: 'name',
                                            label: 'Имя',
                                            type: 'text',
                                            value: 'Вадим Кораблев',
                                        },
                                    ],
                                },
                                {
                                    type: 'nav',
                                },
                            ],
                            content: [
                                {
                                    type: 'post',
                                    date: '5 aug 20',
                                    user: {
                                        name: 'Sasha Kim',
                                        id: '3',
                                    },
                                    body: 'Love you all',
                                    images: [
                                        
                                    ],
                                    link: {
                                        title: '',
                                        href: '',
                                        picture: '',
                                    },
                                    viewed: {
                                    
                                    },
                                    liked: {
                                    
                                    },
                                    comments: [
                                        
                                    ],
                                },
                            ]
                        },
                    },
                    '04-messages': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Сообщения',
                        data: {
                            alts: [
                                {
                                    type: 'nav',
                                },
                            ],
                        },
                    },
                    '05-friends': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Друзья',
                        data: {
                            alts: [
                                {
                                    type: 'nav',
                                },
                            ],
                        },
                    },
                    '06-groups': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Группы',
                        data: {
                            alts: [
                                {
                                    type: 'nav',
                                },
                            ],
                        },
                    },
                    '07-photos': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Фото',
                        data: {
                            alts: [
                                {
                                    type: 'nav',
                                },
                            ],
                        },
                    },
                    '08-settigns': {
                        nav: true,
                        layout: 'layout-double',
                        title: 'Настройки',
                        data: {
                            alts: [
                                {
                                    type: 'nav',
                                },
                            ],
                        },
                    },
                },
                currentPage: 'loader',
                backEvents: {},
            },
            computed: {
                currentLayoutClass: function() {
                    return this.pages[this.currentPage].layout;
                },
                currentPopupLayout: function() {
                    return this.currentPopup.layout;
                },
            },
            mounted: function() {
                
                if (this.settings.key == '') {
                    this.currentPage = this.settings.default_page;
                }
                else {
                    this.currentPage = this.settings.main_page;
                }
                
            },
            methods: {
                getPageClass: function(page) {
                    return (page == this.currentPage) ? 'active' : 'inactive';
                },
                showPage: function(page) {
                    if (this.settings.key == '') {
                        this.currentPage = this.settings.default_page;
                    }
                    else {
                        this.currentPage = page;
                    }
                },
                getTitle: function(title) {
                    console.log('title', title);
                    let parts = title.split('-');
                    return parts[1];
                },
                closePopup: function(popup) {
                    if (this.popups[popup]) {
                        let newPopups = {};
                        for (p in this.popups) {
                            if (p != popup) {
                                newPopups[p] = this.popups[p];
                            }
                        }
                        this.popups = newPopups;
                        // так не работает
                        //delete this.popups[popup];
                    }
                }
            },
        });
    </script>
</html>
